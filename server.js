const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const app = express()
const router = require('./router')
const async = require('async')
const uuidv1 = require('uuid/v1')
const config = require('./config/main')
const port = 3001
const gun = config.gun
const db = config.db

app.disable('x-powered-by')
app.use(helmet())
app.use(helmet.noCache())
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials');
    res.header('Access-Control-Allow-Credentials', 'true');
    next();
})
app.use(db.serve)
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({
      code: 401, message: 'Invalid token...'
    })
  } else if (err.code === 'permission_denied') {
    res.status(403).json({
      code: 403, message: 'Forbidden...'
    })
  }
})
app.use(express.static(__dirname))
router(app)
const server = app.listen(port)
