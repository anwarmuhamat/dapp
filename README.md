How to start?
1. Clone the repo: git clone https://gitlab.com/anwarmuhamat/dapp.git
2. cd dapp
3. Install dependencies: npm install
4. Run the server using .json file: TOP_SECRET='your_top_secret Must be 256 bytes (32 characters)' node server.js
5. Run the server using AWS S3:
AWS_ACCESS_KEY_ID='your_s3_access_key' AWS_SECRET_ACCESS_KEY='your_s3_secret' AWS_S3_BUCKET='your_s3_bucket' TOP_SECRET='your_top_secret Must be 256 bytes (32 characters)' node server.js

Endpoints:
1. Sign-up: POST http://localhost:8080/sign-up body: { "username": "dilan", "password": "dilant0psecret"}
2. Sign-in: POST http://localhost:8080/sign-in body: { "username": "dilan", "password": "dilant0psecret"}
3. Get me: GET http://localhost:8080/user/me header: {'Authorization': 'Bearer jwt_from_login'}
