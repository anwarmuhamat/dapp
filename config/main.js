const db = require('gun')
require('gun/sea')
const sea = db.SEA
const uuidv1 = require('uuid/v1')
const gun = db({
  localStorage: false,
  radisk: true,
  uuid: function () {
    return uuidv1()
  },
  peers: ['http://localhost:8080/gun']
})

module.exports = {
  gun,
  db,
  sea
}
